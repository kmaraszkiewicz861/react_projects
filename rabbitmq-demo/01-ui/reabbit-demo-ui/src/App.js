import logo from './logo.svg';
import './App.css';
import AddForm from './Views/AddForm';

import { BrowserRouter, NavLink, Routes, Route, Navigate } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <div className="App container">
        <nav>
          <ul class="nav">
            <NavLink className="nav-link" aria-current="page" to="/add-form">Dodaj wiadomość</NavLink>
          </ul>
        </nav>

        <div className='row'>
          <Routes>
            <Route path='' element={ <Navigate to="/add-form" /> } />
            <Route path="/add-form" element={ <AddForm /> } />
          </Routes>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
