import React, {useEffect, useState} from "react";
import $ from 'jquery';

function ItemsTable(props) {
    const [items, setItems] = useState([])

    function refresh() {
        $.ajax({
            url: 'http://localhost:5000/api/Database',
            dataType: 'json',
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
            },
            cache: false,
            method: 'get',
            success: (data) => {

                console.log(data.model);

                setItems(data.model)

                console.log(items);
            }
        })
    }

    useEffect(() => {
        refresh();
    })

    if (items.length == 0) {
        return '';
    }

    return(
        <table className="table">
            <thead>
                <tr>
                    <th scope="col">Key</th>
                    <th scope="col">Value</th>
                </tr>
            </thead>
            <tbody>

                {items.map((item, i) => {
                    console.log(item);
                    return (<tr>
                        <th scope="row">{item.key}</th>
                        <td>{item.value}</td>
                    </tr>)
                })}
            </tbody>
        </table>
    )
}

export default ItemsTable;