import React, { useState } from "react";
import $ from 'jquery';
import ItemsTable from './ItemsTable';

// function AddForm(props) {
//     const [wasRequestFinised, setWasRequestFinised] = useState(false);
//     const [isValid, setIsValid] = useState(true);
//     const [isLoaded, setIsLoaded] = useState(true);
//     const [errorMessage, setErrorMessage] = useState('');
//     const [key, setKey] = useState('');
//     const [value, setValue] = useState('');

//     this.handleKeyValue = this.handleKeyValue.bind(this);
//     this.handleValueColumn = this.handleValueColumn.bind(this);
//     this.handleSubmit = this.handleSubmit.bind(this);

//     function handleKeyValue(event) {
//         setKey(event.target.value);
//     }

//     function handleValueColumn(event) {
//         setValue(event.target.value);
//     }

//     function handleSubmit(event) {
//         sendRequest();
//         event.preventDefault();
//     }

//     function renderResponseMessage() {
//         if (this.state.wasRequestFinised && this.state.isValid) {
//             return (
//                 <div class="alert alert-success" role="alert">
//                     Dodano poprawnie wiadomość do bazy Redis
//                 </div>
//             );
//         } else if (this.state.wasRequestFinised && !this.state.isValid) {
//             return (
//                 <div class="alert alert-danger" role="alert">
//                     {this.state.errorMessage}
//                 </div>
//             );
//         }

//         return '';
//     }

//     function sendRequest() {
//         $.ajax({
//             url: 'http://localhost:5000/Redis',
//             dataType: "json",
//             headers: {
//                 "Accept": 'application/json',
//                 "Content-Type": 'application/json'
//             },
//             cache: false,
//             method: 'post',
//             data: JSON.stringify({ key: this.state.key, value: this.state.value }),
//             success: (data) => {

//                 console.log(data);

//                 if (data.isValid) {
//                     setWasRequestFinised(true);
//                     setIsValid(true);

//                     return;
//                 }

//                 setWasRequestFinised(true);
//                 setIsValid(true);
//                 setErrorMessage(data.errorMessage);

//             }, error: (xhr, status, err) => {

//                 setIsLoaded(true)
//                 setWasRequestFinised(true);
//                 setIsValid(false);
//                 setErrorMessage(`Żądanie zaskończone niepowodzeniem ze statusem ${status}`);
//             }
//         })
//     }

//     return(
//         <div className="row">
//             {renderResponseMessage()}
//             <form onSubmit={this.handleSubmit}>
//                 <div className="mb-3">
//                     <label for="keyColumn" className="form-label">Klucz</label>
//                     <input type="text" className="form-control" id="keyColumn" value={key} onChange={this.handleKeyValue} />
//                 </div>
//                 <div class="mb-3">
//                     <label for="valueColumn" className="form-label">Waidomość</label>
//                     <input type="text" className="form-control" id="valueColumn" value={value} onChange={this.handleValueColumn} />
//                 </div>
//                 <div class="mb-3">
//                     <input type="submit" class="btn btn-primary mb-3" value="Zapisz"/>
//                 </div>
//             </form>
//             <div className="row">
//                 <ItemsTable />
//             </div>
//         </div>
//     );
// }

class AddForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            wasRequestFinised: false,
            isValid: true,
            errorMessage: '',
            key: '',
            value: '',
        }

        this.handleKeyValue = this.handleKeyValue.bind(this);
        this.handleValueColumn = this.handleValueColumn.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleKeyValue(event) {
        this.setState({
            key: event.target.value
        })
    }

    handleValueColumn(event) {
        this.setState({
            value: event.target.value
        })
    }

    handleSubmit(event) {
        this.sendRequest();
        event.preventDefault();
    }

    render() {
        return(
            <div className="row">
                {this.renderResponseMessage()}
                <form onSubmit={this.handleSubmit}>
                    <div className="mb-3">
                        <label for="keyColumn" className="form-label">Klucz</label>
                        <input type="text" className="form-control" id="keyColumn" value={this.state.key} onChange={this.handleKeyValue} />
                    </div>
                    <div class="mb-3">
                        <label for="valueColumn" className="form-label">Waidomość</label>
                        <input type="text" className="form-control" id="valueColumn" value={this.state.value} onChange={this.handleValueColumn} />
                    </div>
                    <div class="mb-3">
                        <input type="submit" class="btn btn-primary mb-3" value="Zapisz"/>
                    </div>
                </form>
                <div className="row">
                    <ItemsTable />
                </div>
            </div>
        );
    }

    renderResponseMessage() {
        if (this.state.wasRequestFinised && this.state.isValid) {
            return (
                <div class="alert alert-success" role="alert">
                    Dodano poprawnie wiadomość do bazy Redis
                </div>
            );
        } else if (this.state.wasRequestFinised && !this.state.isValid) {
            return (
                <div class="alert alert-danger" role="alert">
                    {this.state.errorMessage}
                </div>
            );
        }

        return '';
    }

    sendRequest() {
        $.ajax({
            url: 'http://localhost:5000/Redis',
            dataType: "json",
            headers: {
                "Accept": 'application/json',
                "Content-Type": 'application/json'
            },
            cache: false,
            method: 'post',
            data: JSON.stringify({ key: this.state.key, value: this.state.value }),
            success: (data) => {

                console.log(data);

                if (data.isValid) {
                    this.setState({
                        wasRequestFinised: true,
                        isValid: true,
                    });

                    return;
                }

                this.setState({
                    wasRequestFinised: true,
                    isValid: false,
                    errorMessage: data.errorMessage
                });
            }, error: (xhr, status, err) => {

                console.error(xhr);
                console.error(status);
                console.error(err);

                this.setState({
                    isLoaded: true,
                    isValid: false,
                    errorMessage: `Żądanie zaskończone niepowodzeniem ze statusem ${status}`
                });
            }
        })
    }


}

export default AddForm;