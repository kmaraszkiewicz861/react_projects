﻿using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RabbitMQ.Client;
using RabbitmqReaderWorkerService.Services;

namespace RabbitmqReaderWorkerService
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var host = new HostBuilder()
                .ConfigureServices(services =>
                {
                    var factory = new ConnectionFactory() { HostName = "192.168.0.20" };
                    services.AddSingleton(factory);
                    services.AddScoped<IRabbitmqConsumerService, RabbitmqConsumerService>();
                    services.AddHttpClient<IDatabaseHttpService, DatabaseHttpService>();
                });

            var serviceProvider = host.Build().Services;

            var rabbitmqConsumerService = serviceProvider.GetService<IRabbitmqConsumerService>();

            rabbitmqConsumerService.Handle();
        }


    }
}
