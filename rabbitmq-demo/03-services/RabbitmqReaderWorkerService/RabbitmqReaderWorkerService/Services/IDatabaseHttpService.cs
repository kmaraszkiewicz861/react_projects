﻿using System.Threading.Tasks;

namespace RabbitmqReaderWorkerService.Services
{
    public interface IDatabaseHttpService
    {
        Task<Result> SendRequestAsync(string jsonBodyString);
    }
}