﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace RabbitmqReaderWorkerService.Services
{
    public class DatabaseHttpService : IDatabaseHttpService
    {
        private readonly string Url = "http://localhost:5000/api/Database";

        private readonly HttpClient _httpClient;

        public DatabaseHttpService(HttpClient httpClient)
        {
            _httpClient=httpClient;
        }

        public async Task<Result> SendRequestAsync(string jsonBodyString)
        {
            try
            {
                var stringContent = new StringContent(jsonBodyString, Encoding.UTF8, "application/json");

                HttpResponseMessage response = await _httpClient.PostAsync(Url, stringContent);

                if (!response.IsSuccessStatusCode)
                {
                    var jsonResponse = await response.Content.ReadAsStringAsync();

                    return new Result($"Invalid status code was returned: {response.StatusCode} and json body: {jsonResponse}");
                }

                return new Result();
            }
            catch (Exception err)
            {
                return new Result(err.Message);
            }
        }
    }
}
