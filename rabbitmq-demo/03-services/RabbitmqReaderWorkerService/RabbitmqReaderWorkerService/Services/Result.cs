﻿namespace RabbitmqReaderWorkerService.Services
{
    public class Result
    {
        public bool IsValid => string.IsNullOrWhiteSpace(ErrorMessage);

        public string ErrorMessage { get; }

        public Result()
        {

        }

        public Result(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }
    }
}
