﻿using System;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitmqReaderWorkerService.Services
{
    public class RabbitmqConsumerService : IRabbitmqConsumerService
    {
        private readonly ConnectionFactory connectionFactory;

        private readonly IDatabaseHttpService _databaseHttpService;

        public RabbitmqConsumerService(ConnectionFactory connectionFactory, 
            IDatabaseHttpService databaseHttpService)
        {
            this.connectionFactory=connectionFactory;
            _databaseHttpService=databaseHttpService;
        }

        public void Handle()
        {
            using (var connection = connectionFactory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare("redis-key",
                        false,
                        false,
                        false,
                        null);

                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += async (sender, e) => await Consumer_Received(sender, e);

                    channel.BasicConsume("redis-key",
                        true,
                        consumer);

                    Console.WriteLine("Click enter to end consuming queue....");
                    Console.ReadLine();
                }
            }
        }

        private async Task Consumer_Received(object sender, BasicDeliverEventArgs e)
        {
            var bodyInString = Encoding.UTF8.GetString(e.Body.ToArray());

            Console.WriteLine($"Received messege: {bodyInString}");

            var result = await _databaseHttpService.SendRequestAsync(bodyInString);

            Console.WriteLine($"Sending request ended with succes: {result.IsValid} " +
                $"with error message: {result.ErrorMessage}");
        }
    }
}