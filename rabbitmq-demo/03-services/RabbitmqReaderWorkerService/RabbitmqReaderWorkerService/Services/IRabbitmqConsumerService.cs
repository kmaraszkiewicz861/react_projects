﻿using System.Threading.Tasks;

namespace RabbitmqReaderWorkerService.Services
{
    public interface IRabbitmqConsumerService
    {
        void Handle();
    }
}