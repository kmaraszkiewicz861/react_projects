﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using StackExchange.Redis;

namespace RabbitmqWriterService
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IServer _redisServer;
        private readonly IDatabase _redisDatabase;

        public Worker(ILogger<Worker> logger, IServer redisServer, IDatabase redisDatabase)
        {
            _logger = logger;
            _redisServer=redisServer;
            _redisDatabase=redisDatabase;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var factory = new ConnectionFactory { HostName = "192.168.0.20" };
            using (var connection = factory.CreateConnection())
            {

                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);

                using (var channel = connection.CreateModel())
                {
                    var index = 1;
                    while (!stoppingToken.IsCancellationRequested)
                    {
                        channel.QueueDeclare(queue: "redis-key",
                            durable: false,
                            exclusive: false,
                            autoDelete: false,
                            arguments: null);

                        await foreach(RedisKey key in _redisServer.KeysAsync(pattern: "*"))
                        {
                            RedisValue redisValue = await _redisDatabase.ListLeftPopAsync(key);

                            while (redisValue.HasValue)
                            {
                                string message = $"{{\"key\":\"{key}\", \"value\":\"{redisValue}\"}}";
                                var body = Encoding.UTF8.GetBytes(message);

                                channel.BasicPublish(exchange: "",
                                    routingKey: "redis-key",
                                    basicProperties: null,
                                    body: body);

                                Console.WriteLine($"{DateTime.Now}: [x] sent {message}");

                                await Task.Delay(10, stoppingToken);

                                redisValue = await _redisDatabase.ListLeftPopAsync(key);
                            }
                        }

                        Console.WriteLine($"Wait 1 second to check redis database");

                        await Task.Delay(1000, stoppingToken);
                    }
                }
            }
        }
    }
}
