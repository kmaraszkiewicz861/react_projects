using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using StackExchange.Redis;

namespace RabbitmqWriterService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    var redisconnection = ConnectionMultiplexer.Connect("192.168.0.20:6379,allowAdmin=true");
                    var database = redisconnection.GetDatabase();
                    var redisServe = redisconnection.GetServer("192.168.0.20:6379");

                    services.AddSingleton(database);
                    services.AddSingleton(redisServe);

                    services.AddHostedService<Worker>();
                });
    }
}
