﻿using Microsoft.EntityFrameworkCore;
using RabbitmqDemoApi.Entities;

namespace RabbitmqDemoApi.Contexts
{
    public class RedisDbContext : DbContext
    {
        public DbSet<RedisValueEntity> RedisValues { get; set; }

        public DbSet<RedisKeyEntity> RedisKeys { get; set; }

        public RedisDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RedisValueEntity>()
                .HasKey(p => p.Id);

            modelBuilder.Entity<RedisValueEntity>()
                .Property(p => p.Value)
                .IsRequired();

            modelBuilder.Entity<RedisKeyEntity>()
                .HasKey(p => p.Id);

            modelBuilder.Entity<RedisKeyEntity>()
                .Property(p => p.Key)
                .IsRequired();

            base.OnModelCreating(modelBuilder);
        }
    }
}
