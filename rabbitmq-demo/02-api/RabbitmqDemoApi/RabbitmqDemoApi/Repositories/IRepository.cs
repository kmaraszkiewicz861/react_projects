﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Storage;

namespace RabbitmqDemoApi.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> GetAll();
        Task<IDbContextTransaction> BeginTransactionAsync();
        Task<TEntity> InsertAsync(TEntity entity);
        Task SaveChangesAsync();
    }
}