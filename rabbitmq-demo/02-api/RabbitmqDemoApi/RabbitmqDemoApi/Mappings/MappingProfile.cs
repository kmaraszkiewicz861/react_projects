﻿using AutoMapper;
using RabbitmqDemoApi.Handlers.Commands;
using RabbitmqDemoApi.Models;

namespace RabbitmqDemoApi.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<RedisKeyValueRequest, AddKeyValueToDatabaseCommand>();
        }
    }
}