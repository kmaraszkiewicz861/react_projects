﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RabbitmqDemoApi.Migrations
{
    public partial class RabbitmqFirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RedisKeys",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Key = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RedisKeys", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RedisValues",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    RedisKeyEntityId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RedisValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RedisValues_RedisKeys_RedisKeyEntityId",
                        column: x => x.RedisKeyEntityId,
                        principalTable: "RedisKeys",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RedisValues_RedisKeyEntityId",
                table: "RedisValues",
                column: "RedisKeyEntityId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RedisValues");

            migrationBuilder.DropTable(
                name: "RedisKeys");
        }
    }
}
