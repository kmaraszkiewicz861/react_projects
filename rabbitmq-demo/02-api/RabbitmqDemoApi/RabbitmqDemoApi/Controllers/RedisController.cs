﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using RabbitmqDemoApi.Handlers.Commands;
using RabbitmqDemoApi.Handlers.Queries;
using RabbitmqDemoApi.Models;

namespace RabbitmqDemoApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RedisController : ControllerBase
    {
        private readonly IMediator _mediator;

        public RedisController(IMediator mediator)
        {
            _mediator=mediator;
        }

        [HttpPost]
        public async Task<ActionResult<ResultResponse>> AddAsync(RedisValueRequest redisValueRequest)
        {
            var response = await _mediator.Send(new AddRedisValueCommand(redisValueRequest));

            if (response.IsValid)
            {
                return Ok(response);
            }

            return BadRequest(response);
        }

        [HttpGet]
        public async Task<ActionResult<RedisKeyCollection>> GetAllAsync()
        {
            var response = await _mediator.Send(new GetLatestKeysQuery { });

            if (response.IsValid)
            {
                return Ok(response);
            }

            return BadRequest(response);
        }
    }
}