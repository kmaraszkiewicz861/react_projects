﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using RabbitmqDemoApi.Handlers.Commands;
using RabbitmqDemoApi.Handlers.Queries;
using RabbitmqDemoApi.Models;

namespace RabbitmqDemoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DatabaseController : ControllerBase
    {
        private readonly IMediator _mediator;

        private readonly IMapper _mapper;

        public DatabaseController(IMediator mediator, IMapper mapper)
        {
            _mediator=mediator;
            _mapper=mapper;
        }

        [HttpPost]
        public async Task<ActionResult<ResultResponse>> AddKeyWithValueAsync(RedisKeyValueRequest redisKeyValuesRequest)
        {
            var command = _mapper.Map<AddKeyValueToDatabaseCommand>(redisKeyValuesRequest);

            var result = await _mediator.Send(command);

            if (result.IsValid)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpGet]
        public async Task<ActionResult<ResultWithModelResponse<IEnumerable<RedisKeyValueResponse>>>> GetAll()
        {
            var result = await _mediator.Send(new GetAllValuesFromDatabaseQuery { });

            if (result.IsValid)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }
    }
}
