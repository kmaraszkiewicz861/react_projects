﻿namespace RabbitmqDemoApi.Models
{
    public class RedisKeyValueResponse
    {
        public string Key { get; }

        public string Value { get; }

        public RedisKeyValueResponse(string key, string value)
        {
            Key=key;
            Value=value;
        }
    }
}