﻿using System.Linq;

namespace RabbitmqDemoApi.Models
{
    public class RedisValueRequest
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}