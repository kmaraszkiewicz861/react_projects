﻿using System.Collections.Generic;
using FluentValidation.Results;

namespace RabbitmqDemoApi.Models
{
    public class ResultResponse : ResultBase
    {
        protected ResultResponse()
        {

        }

        protected ResultResponse(string errorMessage) : base(errorMessage)
        {
        }

        protected ResultResponse(List<ValidationFailure> validationFailureItems) : base(validationFailureItems)
        {
        }

        public static ResultResponse Ok() => new ResultResponse();

        public static ResultResponse Fail(string errorMessage) => new ResultResponse(errorMessage);

        public static ResultResponse Fail(List<ValidationFailure> validationFailureItems) => new ResultResponse(validationFailureItems);
    }
}