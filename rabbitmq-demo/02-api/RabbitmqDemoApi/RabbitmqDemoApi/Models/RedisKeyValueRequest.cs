﻿namespace RabbitmqDemoApi.Models
{
    public class RedisKeyValueRequest
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}