﻿using System.Collections.Generic;
using System.Linq;

namespace RabbitmqDemoApi.Models
{
    public class RedisKeyValues
    {
        public string Key { get; }

        public string[] Values { get; }

        public RedisKeyValues(string key, string[] values)
        {
            Key=key;
            Values=values;
        }

        public RedisKeyValues(string key, IEnumerable<string> values)
        {
            Key=key;
            Values=values.ToArray() ?? new string[] { };
        }
    }
}