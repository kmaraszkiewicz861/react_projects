﻿using System.Collections.Generic;
using System.Linq;
using FluentValidation.Results;

namespace RabbitmqDemoApi.Models
{
    public abstract class ResultBase
    {
        public string ErrorMessage { get; } = string.Empty;

        public string[] ValidationErrors { get; } = new string[] { };

        public bool IsValid => string.IsNullOrWhiteSpace(ErrorMessage) && ValidationErrors.Length == 0;

        protected ResultBase()
        {

        }

        protected ResultBase(string errorMessage)
        {
            ErrorMessage=errorMessage;
        }

        protected ResultBase(List<ValidationFailure> validationFailureItems)
        {
            ValidationErrors=validationFailureItems.Select(error => error.ErrorMessage).ToArray();
        }
    }
}