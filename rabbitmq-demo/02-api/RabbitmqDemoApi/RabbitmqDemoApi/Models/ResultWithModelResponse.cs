﻿using System.Collections.Generic;
using FluentValidation.Results;

namespace RabbitmqDemoApi.Models
{
    public class ResultWithModelResponse<TModel> : ResultBase
    {
        public TModel Model { get; } = default;

        protected ResultWithModelResponse(TModel model)
        {
            Model = model;
        }

        protected ResultWithModelResponse(string errorMessage) : base(errorMessage)
        {
        }

        protected ResultWithModelResponse(List<ValidationFailure> validationFailureItems) : base(validationFailureItems)
        {
        }

        public static ResultWithModelResponse<TModel> Ok(TModel model) => new ResultWithModelResponse<TModel>(model);

        public static ResultWithModelResponse<TModel> Fail(string errorMessage) => new ResultWithModelResponse<TModel>(errorMessage);

        public static ResultWithModelResponse<TModel> Fail(List<ValidationFailure> validationFailureItems) => new ResultWithModelResponse<TModel>(validationFailureItems);

    }
}