﻿using System.Collections.Generic;

namespace RabbitmqDemoApi.Models
{
    public class RedisKeyCollection
    {
        public IReadOnlyList<RedisKeyValues> Keys { get; }

        public RedisKeyCollection(IReadOnlyList<RedisKeyValues> keys)
        {
            Keys=keys;
        }
    }
}