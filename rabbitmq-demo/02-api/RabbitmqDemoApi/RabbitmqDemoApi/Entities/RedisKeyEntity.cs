﻿using System.Collections.Generic;

namespace RabbitmqDemoApi.Entities
{
    public class RedisKeyEntity
    {
        public int Id { get; set; }

        public string Key { get; set; }

        public ICollection<RedisValueEntity> RedisValues { get; set; }
    }
}