﻿namespace RabbitmqDemoApi.Entities
{
    public class RedisValueEntity
    {
        public int Id { get; set; }

        public string Value { get; set; }

        public RedisKeyEntity RedisKeyEntity { get; set; }
    }
}
