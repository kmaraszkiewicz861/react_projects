﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RabbitmqDemoApi.Models;
using RabbitmqDemoApi.Validators;
using StackExchange.Redis;

namespace RabbitmqDemoApi.Handlers.Commands
{
    public class AddRedisValueCommandHandler : IRequestHandler<AddRedisValueCommand, ResultResponse>
    {
        private readonly IDatabase _redisDatabase;

        public AddRedisValueCommandHandler(IDatabase redisDatabase)
        {
            _redisDatabase=redisDatabase;
        }

        public async Task<ResultResponse> Handle(AddRedisValueCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var validation = await new AddRedisValueCommandValidator().ValidateAsync(request);

                if (!validation.IsValid)
                {
                    return ResultResponse.Fail(validation.Errors);
                }

                await _redisDatabase.ListRightPushAsync(request.Key, request.Value);

                return ResultResponse.Ok();
            }
            catch (Exception err)
            {
                return ResultResponse.Fail(err.Message);
            }

        }
    }
}