﻿using MediatR;
using RabbitmqDemoApi.Models;

namespace RabbitmqDemoApi.Handlers.Commands
{
    public class AddRedisValueCommand : IRequest<ResultResponse>
    {
        public string Key { get; }

        public string Value { get; }

        public AddRedisValueCommand(RedisValueRequest redisValueRequest)
        {
            Key = redisValueRequest.Key;
            Value = redisValueRequest.Value;
        }
    }
}
