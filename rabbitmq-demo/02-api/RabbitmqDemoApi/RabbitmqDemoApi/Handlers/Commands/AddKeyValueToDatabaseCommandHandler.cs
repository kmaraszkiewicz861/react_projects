﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RabbitmqDemoApi.Entities;
using RabbitmqDemoApi.Models;
using RabbitmqDemoApi.Repositories;
using RabbitmqDemoApi.Validators;

namespace RabbitmqDemoApi.Handlers.Commands
{
    public class AddKeyValueToDatabaseCommandHandler : IRequestHandler<AddKeyValueToDatabaseCommand, ResultResponse>
    {
        private readonly IRepository<RedisValueEntity> _redisValueEntityRepository;

        private readonly IRepository<RedisKeyEntity> _redisKeyEntityRepository;

        public AddKeyValueToDatabaseCommandHandler(IRepository<RedisValueEntity> redisValueEntityRepository, IRepository<RedisKeyEntity> redisKeyEntityRepository)
        {
            _redisValueEntityRepository=redisValueEntityRepository;
            _redisKeyEntityRepository=redisKeyEntityRepository;
        }

        public async Task<ResultResponse> Handle(AddKeyValueToDatabaseCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var validator = await new AddKeyValuesToDatabaseCommandValidator().ValidateAsync(request, cancellationToken);

                if (!validator.IsValid)
                {
                    return ResultResponse.Fail(validator.Errors);
                }

                using (var transaction = await _redisKeyEntityRepository.BeginTransactionAsync())
                {
                    RedisKeyEntity key = await _redisKeyEntityRepository.GetAll().FirstOrDefaultAsync(r => r.Key == request.Key);

                    if (key == null)
                    {
                        key = await _redisKeyEntityRepository.InsertAsync(new RedisKeyEntity
                        {
                            Key = request.Key
                        });

                        await _redisKeyEntityRepository.SaveChangesAsync();
                    }

                    await _redisValueEntityRepository.InsertAsync(new RedisValueEntity
                    {
                        Value = request.Value,
                        RedisKeyEntity = key
                    });

                    await _redisValueEntityRepository.SaveChangesAsync();

                    await transaction.CommitAsync();
                }

                return ResultResponse.Ok();
            }
            catch (Exception err)
            {
                return ResultResponse.Fail(err.Message);
            }
        }
    }
}
