﻿using MediatR;
using RabbitmqDemoApi.Models;

namespace RabbitmqDemoApi.Handlers.Commands
{
    public class AddKeyValueToDatabaseCommand : IRequest<ResultResponse>
    {
        public string Key { get; }

        public string Value { get; }

        public AddKeyValueToDatabaseCommand(string key, string value)
        {
            Key=key;
            Value=value;
        }
    }
}
