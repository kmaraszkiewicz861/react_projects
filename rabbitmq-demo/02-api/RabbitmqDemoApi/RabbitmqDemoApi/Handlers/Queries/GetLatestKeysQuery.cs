﻿using MediatR;
using RabbitmqDemoApi.Models;

namespace RabbitmqDemoApi.Handlers.Queries
{
    public class GetLatestKeysQuery : IRequest<ResultWithModelResponse<RedisKeyCollection>>
    {
    }
}