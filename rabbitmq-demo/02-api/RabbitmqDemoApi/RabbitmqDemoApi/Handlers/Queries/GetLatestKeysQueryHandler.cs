﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RabbitmqDemoApi.Models;
using StackExchange.Redis;

namespace RabbitmqDemoApi.Handlers.Queries
{
    public class GetLatestKeysQueryHandler : IRequestHandler<GetLatestKeysQuery, ResultWithModelResponse<RedisKeyCollection>>
    {
        private readonly IDatabase _redisDatabase;
        private readonly IServer _redisServer;

        public GetLatestKeysQueryHandler(IDatabase redisDatabase, IServer redisServer)
        {
            _redisDatabase=redisDatabase;
            _redisServer=redisServer;
        }

        public async Task<ResultWithModelResponse<RedisKeyCollection>> Handle(GetLatestKeysQuery request, CancellationToken cancellationToken)
        {
            try
            {
                List<string> values = new List<string>();
                List<RedisKeyValues> redisKeys = new List<RedisKeyValues>();

                await foreach (RedisKey redisKey in _redisServer.KeysAsync(pattern: "*"))
                {
                    RedisValue redisValue = RedisValue.Null;

                    do
                    {
                        redisValue = await _redisDatabase.ListLeftPopAsync(redisKey);

                        if (redisValue.HasValue)
                        {
                            values.Add(redisValue);
                        }
                    }
                    while (redisValue.HasValue);

                    redisKeys.Add(new RedisKeyValues(redisKey.ToString(), values.ToArray()));
                }

                return ResultWithModelResponse<RedisKeyCollection>.Ok(new RedisKeyCollection(redisKeys));
            }
            catch (Exception err)
            {
                return ResultWithModelResponse<RedisKeyCollection>.Fail(err.Message);
            }
        }
    }
}
