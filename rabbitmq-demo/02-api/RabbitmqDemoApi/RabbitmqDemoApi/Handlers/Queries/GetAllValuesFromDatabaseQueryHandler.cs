﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RabbitmqDemoApi.Entities;
using RabbitmqDemoApi.Models;
using RabbitmqDemoApi.Repositories;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace RabbitmqDemoApi.Handlers.Queries
{
    public class GetAllValuesFromDatabaseQueryHandler : IRequestHandler<GetAllValuesFromDatabaseQuery, ResultWithModelResponse<IEnumerable<RedisKeyValueResponse>>>
    {
        private readonly IRepository<RedisValueEntity> _redisValueEntityRepository;

        private readonly IRepository<RedisKeyEntity> _redisKeyEntityRepository;

        public GetAllValuesFromDatabaseQueryHandler(IRepository<RedisValueEntity> redisValueEntityRepository, IRepository<RedisKeyEntity> redisKeyEntityRepository)
        {
            _redisValueEntityRepository=redisValueEntityRepository;
            _redisKeyEntityRepository=redisKeyEntityRepository;
        }

        public async Task<ResultWithModelResponse<IEnumerable<RedisKeyValueResponse>>> Handle(GetAllValuesFromDatabaseQuery request, CancellationToken cancellationToken)
        {
            var keys = _redisKeyEntityRepository.GetAll();
            var values = _redisValueEntityRepository.GetAll();

            var keysWithValues = await keys.Join(values, outer => outer.Id, inner => inner.RedisKeyEntity.Id, (outer, inner) => new { Key = outer.Key, Value = inner.Value})
                .Select(item => new RedisKeyValueResponse(item.Key, item.Value))
                .ToListAsync();

            return ResultWithModelResponse<IEnumerable<RedisKeyValueResponse>>.Ok(keysWithValues);
        }
    }
}