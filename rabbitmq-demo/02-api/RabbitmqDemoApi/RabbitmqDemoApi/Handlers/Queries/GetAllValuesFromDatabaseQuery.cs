﻿using System.Collections.Generic;
using MediatR;
using RabbitmqDemoApi.Models;

namespace RabbitmqDemoApi.Handlers.Queries
{
    public class GetAllValuesFromDatabaseQuery : IRequest<ResultWithModelResponse<IEnumerable<RedisKeyValueResponse>>>
    {

    }
}
