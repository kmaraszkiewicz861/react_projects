﻿using FluentValidation;
using RabbitmqDemoApi.Handlers.Commands;

namespace RabbitmqDemoApi.Validators
{
    public class AddKeyValuesToDatabaseCommandValidator : AbstractValidator<AddKeyValueToDatabaseCommand>
    {
        public AddKeyValuesToDatabaseCommandValidator()
        {
            RuleFor(p => p.Key)
                .NotEmpty()
                .WithMessage("Te pole (Klucz) jest istone. Wypełnij je albo sobie czytaj ten błąd");

            RuleFor(p => p.Value)
                .NotEmpty()
                .WithMessage("Te pole (Wartość klucza) jest istone. Wypełnij je albo sobie czytaj ten błąd");
        }
    }
}