using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using StackExchange.Redis;
using MediatR;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using RabbitmqDemoApi.Contexts;
using RabbitmqDemoApi.Repositories;
using RabbitmqDemoApi.Entities;
using AutoMapper;

namespace RabbitmqDemoApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            var multiplexer = ConnectionMultiplexer.Connect("192.168.0.20:6379,allowAdmin=true");
            var redisDatabase = multiplexer.GetDatabase();
            var redisServer = multiplexer.GetServer("192.168.0.20:6379");

            services.AddSingleton<IDatabase>(redisDatabase);
            services.AddSingleton<IServer>(redisServer);
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddDbContext<RedisDbContext>(options =>
                options.UseSqlServer(Configuration["ConnectionString"]));
            services.AddScoped<IRepository<RedisValueEntity>, Repository<RedisValueEntity>>();
            services.AddScoped<IRepository<RedisKeyEntity>, Repository<RedisKeyEntity>>();

            services.AddCors(options =>
            {
                options.AddDefaultPolicy(builder =>
                {
                    builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                });
            });

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "RabbitmqDemoApi", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "RabbitmqDemoApi v1"));
            }

            app.UseRouting();

            app.UseCors();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
