import $ from 'jquery';

class RedisValueHttpClient {
    addValueToRedis(data, success, error) {
        $.ajax({
            url: 'http://localhost:10864/Redis',
            dataType: 'json',
            cache: false,
            method: 'post',
            data: JSON.stringify({
                key: data.key,
                value: data.value
              }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            success: success, 
            error: error
        })
    }

    getData(success, error) {
        $.ajax({
            url: 'http://localhost:10864/Redis',
            dataType: 'json',
            cache: false,
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            success: success, 
            error: error
        })
    }
}

export default RedisValueHttpClient;