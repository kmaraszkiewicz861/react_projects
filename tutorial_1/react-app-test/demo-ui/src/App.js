import logo from './logo.svg';
import { BrowserRouter, Route, Routes, NavLink, Navigate } from 'react-router-dom';

import './App.css';
import AddForm from './Views/AddForm';
import KeyList from './Views/KeyList';

function App() {
  return (
    <BrowserRouter>
      <div className="App container">
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <div className="container-fluid">
            <a className="navbar-brand" href="#">Navbar</a>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="/add-form">Form</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/redis-keys">
                    List of redis keys
                  </NavLink>
                </li>
              </ul>
            </div>
          </div>
        </nav>

        <Routes>
          <Route path="/add-form" element={ <AddForm />}  />
          <Route path="/redis-keys" element={ <KeyList /> } />
          <Route path="/" element={<Navigate to="/redis-keys" />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
