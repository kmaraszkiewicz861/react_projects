import React from "react";
import RedisValueHttpClient from './../Services/RedisValueHttpClient';

class KeyList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false,
            values: []
        }
    }

    componentDidMount() {
        let service = new RedisValueHttpClient();

        service.getData((data) => {
            this.setState({
                values: data.model.values,
                isLoaded: true
            });

            console.log(data.model.values);
            console.log(this.state.values);
        }, (xhr, status, err) => {
            console.error(err);
            console.log(status);
        })
    }

    render() {

        console.log(this.state)

        if (!this.state.isLoaded) {
            return (
                <div class="alert alert-primary" role="alert">
                    Page is loading... Please wait :)
                </div>
            )
        }

        let items = this.state.values;

        

        return (
            <div className="row">
                <table className="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Klucz</th>
                        <th scope="col">Wiadomość</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            items.map((data, index) => {
                                return (
                                    <tr>
                                    <th scope="row">{index}</th>
                                    <th>{data.key}</th>
                                    <td>{data.value}</td>
                                </tr> 
                                )
                            })
                        }
                    </tbody>
                    </table>
            </div>
        )
    }
}

export default KeyList;