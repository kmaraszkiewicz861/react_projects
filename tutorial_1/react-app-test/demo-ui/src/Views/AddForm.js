import React from "react";
import RedisValueHttpClient from './../Services/RedisValueHttpClient';
import RequestMessage from './RequestMessage';

class AddForm extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            key: '',
            value: '',
            isLoading: false,
            isRequestMessageVisible: false,
            requestMessageType: '',
            requestMessage: ''
        }

        this.handleKey = this.handleKey.bind(this);
        this.handleValue = this.handleValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleKey(event) {
        this.setState({ key: event.target.value })
    }

    handleValue(event) {
        this.setState({ value: event.target.value })
    }

    handleSubmit(event) {
        this.setState({ isLoading: true });

        new RedisValueHttpClient().addValueToRedis(this.state, (data) => {
            this.setState({ isLoading: false });

            this.setState({ 
                isRequestMessageVisible: true,
                requestMessageType: 'success',
                requestMessage: 'Klucz poprawnie dodany do Redis',
                isLoading: false
            });

        }, (xhr, status, err) => {

            this.setState({ 
                isRequestMessageVisible: true,
                requestMessageType: 'danger',
                requestMessage: `Żądanie zakończyło się z błędnym statusem: ${status}`,
                isLoading: false
            });

            console.error(err);
            console.log(this.state);
        });
    }

    render() {

        if (this.state.isLoading) {
            return (
                <div class="alert alert-primary" role="alert">
                    Page is loading... Please wait :)
                </div>
            )
        }

        return (

            <div className="row">
                <div className="row">
                    <RequestMessage type={this.state.requestMessageType} message={this.state.requestMessage} isVisible={this.state.isRequestMessageVisible} />
                </div>

                <form className="row g-3" onSubmit={this.handleSubmit}>

                    <div class="mb-3">
                        <label for="key" className="form-label">Klucz wiadomości</label>
                        <input type="text" className="form-control" id="key" value={this.state.key} onChange={this.handleKey} />
                    </div>
                    <div class="mb-3">
                        <label for="value" className="form-label">Ciało wiadomości</label>
                        <input type="text" className="form-control" id="value" value={this.state.value} onChange={this.handleValue} />
                    </div>
                    {this.renderButton()}
                </form>
            </div>
        )
    }

    renderButton() {
        if (this.state.key != undefined && this.state.value != undefined
            && this.state.key != '' && this.state.value != '') {
                return(
                    <div class="mb-3">
                        <button type="submit" class="btn btn-primary mb-3">Zapisz w redis!</button>
                    </div>
                );
            }
        
            return '';
    }
}

export default AddForm;