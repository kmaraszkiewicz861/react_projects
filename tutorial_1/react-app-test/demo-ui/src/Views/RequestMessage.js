import React from "react";

class RequestMessage extends React.Component {
    constructor(props) {

        super(props);

        console.log(props);

        this.state = {
            isVisible: false,
            type: props.type,
            message: props.message,
        }
    }

    render() {
        return(
            <div className={this.getVisibilityClass()} role="alert">
                {this.state.message}
            </div>
        );
    }

    getVisibilityClass() {
        return this.state.isVisible ? `alert alert-${this.state.type} visible` : `alert alert-${this.state.type} invisible`;
    }
}

export default RequestMessage;