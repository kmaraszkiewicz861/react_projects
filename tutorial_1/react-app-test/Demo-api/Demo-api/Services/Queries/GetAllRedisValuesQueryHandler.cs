﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Demo_api.Models;
using MediatR;
using StackExchange.Redis;

namespace Demo_api.Services.Queries
{
    public class GetAllRedisValuesQueryHandler : IRequestHandler<GetAllRedisValuesQuery, ResultResponse<RedisValuesResponse>>
    {
        public IDatabase _database;
        public IServer _server;

        public GetAllRedisValuesQueryHandler(IDatabase database, IServer server)
        {
            _database=database;
            _server=server;
        }

        public async Task<ResultResponse<RedisValuesResponse>> Handle(GetAllRedisValuesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                RedisValuesResponse redisValuesResponse = new RedisValuesResponse();

                await foreach (RedisKey redisKey in _server.KeysAsync(pattern: "*"))
                {
                    RedisValue[] items = await _database.ListRangeAsync(redisKey);

                    redisValuesResponse.AddValues(redisKey.ToString(), items.ToList());
                }

                return ResultResponse<RedisValuesResponse>.Ok(redisValuesResponse);
            }
            catch (Exception err)
            {
                return ResultResponse<RedisValuesResponse>.Fail(err.Message);
            }
        }
    }
}