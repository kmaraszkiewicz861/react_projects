﻿using Demo_api.Models;
using MediatR;

namespace Demo_api.Services.Queries
{
    public class GetAllRedisValuesQuery : IRequest<ResultResponse<RedisValuesResponse>>
    {
    }
}