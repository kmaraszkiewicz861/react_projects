﻿using Demo_api.Models;
using MediatR;

namespace Demo_api.Services.Commands
{
    public class AddRedisValueCommand : IRequest<ResultResponse<EmptyResponse>>
    {
        public string Key { get; }

        public string Value { get; }

        public AddRedisValueCommand(RedisRequest redisRequest)
        {
            Key=redisRequest.Key;
            Value=redisRequest.Value;
        }
    }
}