﻿using System.Threading;
using System.Threading.Tasks;
using Demo_api.Models;
using Demo_api.Validators;
using MediatR;
using StackExchange.Redis;

namespace Demo_api.Services.Commands
{
    public class AddResisValueCommandHandler : IRequestHandler<AddRedisValueCommand, ResultResponse<EmptyResponse>>
    {
        private IDatabase database;

        public AddResisValueCommandHandler(IDatabase database)
        {
            this.database=database;
        }

        public async Task<ResultResponse<EmptyResponse>> Handle(AddRedisValueCommand request, CancellationToken cancellationToken)
        {
            var validator = new AddRedisValueCommandValidator();
            var result = await validator.ValidateAsync(request);

            if (!result.IsValid)
            {
                return ResultResponse<EmptyResponse>.Fail(result.Errors);
            }

            database.ListLeftPush(request.Key, request.Value);

            return ResultResponse<EmptyResponse>.Ok();
        }
    }
}