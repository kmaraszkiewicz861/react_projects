﻿using System.Collections.Generic;
using System.Linq;
using FluentValidation.Results;

namespace Demo_api.Models
{
    public class ResultResponse<TModel>
    {
        public string ErrorMessage { get; } = string.Empty;

        public string[] ValidationErrors { get; } = new string[] { };

        public TModel Model { get; } = default;

        public bool IsValid => string.IsNullOrWhiteSpace(ErrorMessage) && ValidationErrors.Length == 0;

        protected ResultResponse()
        {

        }

        protected ResultResponse(string errorMessage)
        {
            ErrorMessage=errorMessage;
        }

        protected ResultResponse(string[] validationErrors)
        {
            ValidationErrors=validationErrors;
        }

        protected ResultResponse(TModel model)
        {
            Model=model;
        }

        public static ResultResponse<TModel> Ok(TModel model)
        {
            return new ResultResponse<TModel>(model);
        }

        public static ResultResponse<TModel> Ok()
        {
            return new ResultResponse<TModel>();
        }

        public static ResultResponse<TModel> Fail(string errorMessage)
        {
            return new ResultResponse<TModel>(errorMessage);
        }

        public static ResultResponse<TModel> Fail(List<ValidationFailure> validationFailures)
        {
            return new ResultResponse<TModel>(validationFailures.Select(error => error.ErrorMessage).ToArray());
        }
    }
}
