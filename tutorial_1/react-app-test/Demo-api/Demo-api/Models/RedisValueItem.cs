﻿namespace Demo_api.Models
{
    public class RedisValueItem
    {
        public string Key { get; }

        public string Value { get; }

        public RedisValueItem(string key, string value)
        {
            Key=key;
            Value=value;
        }
    }
}