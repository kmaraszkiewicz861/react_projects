﻿using System.Collections.Generic;
using System.Linq;
using StackExchange.Redis;

namespace Demo_api.Models
{
    public class RedisValuesResponse
    {
        public List<RedisValueItem> Values { get; } = new List<RedisValueItem>();

        public void AddValues(string key, List<RedisValue> redisValues)
        {
            var valuesStrings = redisValues.Select(i => i.ToString()).ToList();

            foreach (var value in valuesStrings)
            {
                Values.Add(new RedisValueItem(key, value));
            }
        }
    }
}