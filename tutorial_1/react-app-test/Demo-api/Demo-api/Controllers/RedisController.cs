﻿using System.Threading.Tasks;
using Demo_api.Models;
using Demo_api.Services.Commands;
using Demo_api.Services.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Demo_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RedisController : ControllerBase
    {
        private readonly IMediator _mediator;

        public RedisController(IMediator mediator)
        {
            _mediator=mediator;
        }

        [HttpGet]
        public async Task<ActionResult<ResultResponse<RedisValuesResponse>>> GetAllAsync()
        {
            ResultResponse<RedisValuesResponse> result
                = await _mediator.Send(new GetAllRedisValuesQuery());

            if (result.IsValid)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpPost]
        public async Task<ActionResult<ResultResponse<EmptyResponse>>> AddValue(RedisRequest redisRequest)
        {
            ResultResponse<EmptyResponse> result 
                = await _mediator.Send(new AddRedisValueCommand(redisRequest));

            if (result.IsValid)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }
    }
}