﻿using Demo_api.Services.Commands;
using Demo_api.Services.Queries;
using FluentValidation;

namespace Demo_api.Validators
{
    public class AddRedisValueCommandValidator : AbstractValidator<AddRedisValueCommand>
    {
        public AddRedisValueCommandValidator()
        {
            RuleFor(p => p.Key)
                .NotEmpty()
                .WithMessage("Klucz nie może być pusty, ty dzbanie jeden!")
                .MaximumLength(100)
                .WithMessage("Ile tych znaków chcesz wpisać? Do 100 znaków max!");

            RuleFor(p => p.Value)
                .NotEmpty()
                .WithMessage("Wartość kluczanie nie może być pusty, ty dzbanie jeden!")
                .MaximumLength(200)
                .WithMessage("Ile tych znaków chcesz wpisać? Do 200 znaków max!");
        }
    }
}