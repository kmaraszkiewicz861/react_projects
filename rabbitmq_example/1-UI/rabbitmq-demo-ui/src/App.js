import logo from './logo.svg';
import './App.css';
import AddFormRenderer from './Views/AddForm';
import AddForm from './Views/AddForm';
import MessageList from './Views/MessageList';

import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
      <AddForm />
      <MessageList />
    </div>
  );
}

export default App;