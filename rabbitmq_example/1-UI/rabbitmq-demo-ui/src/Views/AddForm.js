import React from 'react';

import { Form, Button } from 'react-bootstrap';

class AddForm extends React.Component {
    render() {
        return (
            <Form>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Enter some message</Form.Label>
                <Form.Control type="email" placeholder="Enter email" />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Send
                </Button>
          </Form>
        );
    }
}

export default AddForm;