﻿using Microsoft.AspNetCore.Mvc;
using MediatR;
using RabbitmqDemoApiShared.Models;
using RabbitmqDemoApiShared.Handlers;

namespace RabbitmqDemoApi.Controllers;

[ApiController]
[Route("[controller]")]
public class RedisDatabaseController : ControllerBase
{
    private readonly IMediator _mediator;

    public RedisDatabaseController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet(Name = "GetWeatherForecast")]
    public async Task<ResultMessage> StorageMessage(RedisMessage redisMessage)
    {
        return await _mediator.Send(new AddMessageToRedisCommand(redisMessage));
    }
}

