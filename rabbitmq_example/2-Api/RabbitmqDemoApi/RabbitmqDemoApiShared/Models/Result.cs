﻿using FluentValidation.Results;

namespace RabbitmqDemoApiShared.Models
{
    public class ResultMessage
	{
        public string ErrorMessage { get; }

		public bool IsValid => !string.IsNullOrWhiteSpace(ErrorMessage)
			|| ValidationErrors.Count > 0;

		public List<string> ValidationErrors { get; }

        private ResultMessage()
		{
			ErrorMessage = string.Empty;
			ValidationErrors = new List<string>();
		}

		private ResultMessage(string errorMessage)
        {
			ErrorMessage = errorMessage;
			ValidationErrors = new List<string>();
		}

        private ResultMessage(List<string> validationErrors)
        {
			ErrorMessage = string.Empty;
			ValidationErrors = validationErrors;
        }

		public static ResultMessage Ok() => new ResultMessage();

		public static ResultMessage Fail(string errorMessage) => new ResultMessage(errorMessage);

		public static ResultMessage Fail(List<ValidationFailure> validationErrors)
			=> new ResultMessage(validationErrors.Select(error => error.ErrorMessage).ToList());
	}
}

