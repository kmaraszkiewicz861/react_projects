﻿using System;
namespace RabbitmqDemoApiShared.Models
{
	public class RedisMessage
	{
        public string Key { get; set; } = string.Empty;

        public string Message { get; set; } = string.Empty;
    }
}