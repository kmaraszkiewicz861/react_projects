﻿using System;
using MediatR;
using RabbitmqDemoApiShared.Models;
using RabbitmqDemoApiShared.Validators;
using StackExchange.Redis;

namespace RabbitmqDemoApiShared.Handlers
{
    public class AddMessageToRedisCommnadHandler : IRequestHandler<AddMessageToRedisCommand, ResultMessage>
	{
        private readonly IDatabase _redisDatabase;

        public AddMessageToRedisCommnadHandler(IDatabase redisDatabase)
        {
            _redisDatabase = redisDatabase;
        }

        public async Task<ResultMessage> Handle(AddMessageToRedisCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var validation = await new AddMessageToRedisCommandValidator()
                    .ValidateAsync(request);

                if (!validation.IsValid)
                    return ResultMessage.Fail(validation.Errors);

                await _redisDatabase.StringSetAsync(request.Key, request.Message);

                return ResultMessage.Ok();

            }
            catch (Exception err)
            {
                return ResultMessage.Fail(err.Message);
            }
        }
    }
}

