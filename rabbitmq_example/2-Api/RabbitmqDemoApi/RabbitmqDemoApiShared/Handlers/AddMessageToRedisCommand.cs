﻿using MediatR;
using RabbitmqDemoApiShared.Models;

namespace RabbitmqDemoApiShared.Handlers
{
    public class AddMessageToRedisCommand : IRequest<ResultMessage>
    {
        public string Key { get; }

        public string Message { get; }

        public AddMessageToRedisCommand(RedisMessage redisMessage)
        {
            Key = redisMessage.Key;
            Message = redisMessage.Message;
        }
    }
}