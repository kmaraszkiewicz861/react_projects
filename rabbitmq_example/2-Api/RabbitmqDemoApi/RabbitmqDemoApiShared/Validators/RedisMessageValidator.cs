﻿using FluentValidation;
using RabbitmqDemoApiShared.Handlers;

namespace RabbitmqDemoApiShared.Validators
{
    public class AddMessageToRedisCommandValidator: AbstractValidator<AddMessageToRedisCommand>
	{
        public AddMessageToRedisCommandValidator()
        {
            RuleFor(p => p.Key)
                .NotEmpty()
                .MaximumLength(200);

            RuleFor(p => p.Message)
                .NotEmpty()
                .MaximumLength(2000);
        }
	}
}

