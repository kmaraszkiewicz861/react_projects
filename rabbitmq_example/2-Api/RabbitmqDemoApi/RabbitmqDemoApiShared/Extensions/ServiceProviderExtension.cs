﻿using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;
using MediatR;
using System.Reflection;

namespace RabbitmqDemoApiShared.Extensions
{
    public static class ServiceProviderExtension
	{
		public static IServiceCollection ConfigureSharedServices(this IServiceCollection services)
        {
			services.AddSingleton(GetDatabase());
			services.AddMediatR(Assembly.GetExecutingAssembly());

			return services;
        }

		private static IDatabase GetDatabase()
        {
			var connection = ConnectionMultiplexer.Connect("localhost:6379");

			return connection.GetDatabase();
        }
	}
}